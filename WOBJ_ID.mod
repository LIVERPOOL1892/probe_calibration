MODULE WOBJ_IDENTIFICATION
    !***********************************************************
    !
    ! Module:  WOBJ_IDENTIFICATION
    !
    ! Description of procedure: 
    !
    !   This program generates three planes, that subsequently returns the intersection point among the three of them.
    !   It generates a common frame, with pose. This point will be defined as the Work Object Frame.
    !   The rotation of the component wrt World is also calculated. 
    !
    ! Requirements  
    !
    !   Probing system
    !   Probing tool calibrated
    !   Reachability for the three surfaces
    !   An empty Frame
    !
    ! Tips
    !
    !   Place the probing sphere within 3cms from the desired surface.
    !
    ! Options
    !
    !   Future work, could provide the method to utilize force control to identify the frame.
    !
    ! Author: walter-frank
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    
    
    
    ! Variables
    !***********************************************************
    !
      
      
    ! Diameter of probing ball
      VAR num Probe_Diameter := 3;
    
    ! Equation Variables
      VAR num Equation_1{4} := [0,0,0,0];
      VAR num Equation_2{4} := [0,0,0,0];
      VAR num Equation_3{4} := [0,0,0,0];
      
    ! Initialize Positions
      VAR robtarget pFound;
      VAR robtarget pEndPoint;
      VAR robtarget pTopFace;
      VAR robtarget pFrontFace;
      VAR robtarget pSideFace;

      
      !!Points on Faces
      VAR robtarget pFace1_P1;
      VAR robtarget pFace1_P2;
      VAR robtarget pFace1_P3;
      
      VAR robtarget pFace2_P1;
      VAR robtarget pFace2_P2;
      VAR robtarget pFace2_P3;
      
      VAR robtarget pFace3_P1;
      VAR robtarget pFace3_P2;
      VAR robtarget pFace3_P3;
      
      !! Vectors on Face
      VAR pos Vector_1_Face1; 
      VAR pos Vector_2_Face1;
      
      VAR pos Vector_1_Face2; 
      VAR pos Vector_2_Face2;
      
      VAR pos Vector_1_Face3; 
      VAR pos Vector_2_Face3;
      
      !! Normal Vector to Plane
      VAR pos Normal_Vector_Face1; 
      
      VAR pos Normal_Vector_Face2;
      
      VAR pos Normal_Vector_Face3; 
      
      !! D Coefficient 
      
      VAR num D{3} := [0,0,0];
      
      !! Determinant
      VAR num det := 0;
      
      !! matrix
      VAR pos A_Row_1;
      VAR pos A_Row_2;
      VAR pos A_Row_3;
      
      !!Inverse matrix with cofactr
      VAR pos Ainv_Row_1_CoFact;
      VAR pos Ainv_Row_2_CoFact;
      VAR pos Ainv_Row_3_CoFact;
      
      !!Adjugating matrix
      VAR pos Adjugate_Row_1;
      VAR pos Adjugate_Row_2;
      VAR pos Adjugate_Row_3;
      
      !! Matrix
      VAR pos A_Inv_Row_1;
      VAR pos A_Inv_Row_2;
      VAR pos A_Inv_Row_3;
      
      !! Temp Coordinates for work object
      VAR num x_temp;
      VAR num y_temp;
      VAR num z_temp;
      
      !! Temporal Work object frame
      VAR wobjdata wobj_temp;
      VAR wobjdata wobj1 := [ FALSE, TRUE, "", [ [0, 0, 0], [1, 0, 0 ,0] ], [ [0, 0, 0], [1, 0, 0 ,0] ] ];
      
      !! For teaching rotation
      VAR pos X1_Pos;
      VAR pos Y1_Pos;
      
      !! Finding angles
      VAR num angle_Z;
      VAR num angle_Y;
      VAR num angle_X;

      !!Origin Planes
      VAR pos PlaneXY := [1,1,0];
      VAR pos PlaneXZ := [1,0,1];
      VAR pos PlaneYZ := [0,1,1];
      
      VAR pos CrossProduct := [0,0,0];
      
     
      !!Temporal tool
      !!PERS tooldata tCalibTemp:=[TRUE,[[-19.6916,-0.334428,533.568],[1,0,0,0]],[53.5,[-35.4,0.5,227.3],[1,0,0,0],4.097,5.318,0]];
      !!Initialize tool
      !!PERS tooldata tMeasure;
          
      
    !
    !***********************************************************
    
    
  
    
    PROC WOBJ_ID()
        
    TPWrite "Welcome to the Work Object Calibration Program"; 
    TPWrite "Do not forget to Calibrate the TCP Probe Tool correctly"; 
       
    TPWrite "It is recommended to jog the robot towards the required faces"; 
    
    COMMENCE:
    reg5 := 0;
    TPReadFK reg5,"Ready","Yes!",stEmpty,stEmpty,stEmpty,"No!"; !!\MaxTime:=60\BreakFlag:= errvar; !!In case of error flag display

     !OR errvar = ERR_TP_MAXTIME
    IF reg5 = 1 THEN
        !! Starts the clock
        CLKStart clocktime;
        !! Stores location as ptop
        pTopFace := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
        reg5 := 0;
        
        !! Goes to routine, where all logic for the probe motion is given
        progFace1;
        progFace2;
        progFace3;
        
        !! Once the points are known, a calculation for the surface plane is performed
        
        !! First, calculate vectors
        Vector_1_Face1 := Step1 (pFace1_P1,pFace1_P2);
        Vector_2_Face1 := Step1 (pFace1_P1,pFace1_P3);
        
        Vector_1_Face2 := Step1 (pFace2_P1,pFace2_P2);
        Vector_2_Face2 := Step1 (pFace2_P1,pFace2_P3);
        
        Vector_1_Face3 := Step1 (pFace3_P1,pFace3_P2);
        Vector_2_Face3 := Step1 (pFace3_P1,pFace3_P3);
        
        !! Cross-Product is needed now
        Normal_Vector_Face1 := Step2(Vector_1_Face1,Vector_2_Face1);
        
        Normal_Vector_Face2 := Step2(Vector_1_Face2,Vector_2_Face2);
        
        Normal_Vector_Face3 := Step2(Vector_1_Face3,Vector_2_Face3);
        
        !! Equation of the plane || Defining D coeficient
        TPErase;
        TPWrite "Approx. Equation of planes"; 
        TPWrite "Equation top planes"; 
        D{1} := Step3 (Normal_Vector_Face1,pFace1_P1);
        D{2} := Step3 (Normal_Vector_Face2,pFace2_P1);
        D{3} := Step3 (Normal_Vector_Face3,pFace3_P1);
        
        
        !! Intersection point Calculation starts here
        
        !! We need to generate the inverse matrix to solve the linear equations
        !! First step is to find the determinant
        
        det := Matrix_Determinant (Normal_Vector_Face1,Normal_Vector_Face2,Normal_Vector_Face3);
        
        !! Development of Matrix of minors
        
        A_Row_1 := Matrix_of_Minors_Row1 (Normal_Vector_Face1,Normal_Vector_Face2,Normal_Vector_Face3);
        A_Row_2 := Matrix_of_Minors_Row2 (Normal_Vector_Face1,Normal_Vector_Face2,Normal_Vector_Face3);
        A_Row_3 := Matrix_of_Minors_Row3 (Normal_Vector_Face1,Normal_Vector_Face2,Normal_Vector_Face3);
        
        !! Cofactors
        
        Ainv_Row_1_CoFact := CoFact_1and3(A_Row_1);
        Ainv_Row_2_CoFact := CoFact_2(A_Row_2);
        Ainv_Row_3_CoFact := CoFact_1and3(A_Row_3);
        
        !! Adjugate Matrix
        
        Adjugate_Row_1 := Adjugating_Row_1(Ainv_Row_1_CoFact,Ainv_Row_2_CoFact,Ainv_Row_3_CoFact);
        Adjugate_Row_2 := Adjugating_Row_2(Ainv_Row_1_CoFact,Ainv_Row_2_CoFact,Ainv_Row_3_CoFact);
        Adjugate_Row_3 := Adjugating_Row_3(Ainv_Row_1_CoFact,Ainv_Row_2_CoFact,Ainv_Row_3_CoFact);
        
        !!Obtaining inverse matrix by multiplying by 1/Determinant
        
        A_Inv_Row_1 := Inverse_Row(Adjugate_Row_1,det);
        A_Inv_Row_2 := Inverse_Row(Adjugate_Row_2,det);
        A_Inv_Row_3 := Inverse_Row(Adjugate_Row_3,det);
             
        !! Calculating work object
        
        x_temp := Eq_Solver(A_Inv_Row_1,D);
        y_temp := Eq_Solver(A_Inv_Row_2,D);
        z_temp := Eq_Solver(A_Inv_Row_3,D);
        
        !! Defining work object frame coordinates
        wobj_temp.uframe.trans.x := x_temp;
        wobj_temp.uframe.trans.y := y_temp;
        wobj_temp.uframe.trans.z := z_temp;
        
        
        !! Calculating Final Work Object value
        wobj1 := CompensationVector(wobj_temp,Probe_Diameter,pFace1_P1,pFace2_P1,pFace3_P1);
        
    ENDIF
    
    IF reg5 = 5 THEN !!In case it is not ready
        MoveAbsJ jhome,v20,z1,tool0\WObj:=wobj0;
        reg5 := 0;
        GOTO COMMENCE;
    ENDIF
          
    ConfL\Off;
        
        
        
    ENDPROC
    
    PROC progFace1()
        
        TPErase;
        reg4 := 0;
        TPReadFK reg4,"Locate the probe on top of the first face","Done",stEmpty,stEmpty,stEmpty,stEmpty; 
        
        IF reg4 = 1 THEN
           !First Point of First Face
           pTopFace := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace1_P1, offs(pTopFace,0,0,-10), v5, tCalibTemp\WObj:=wobj0;
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
           !Second Point of First Face
           MoveL Offs(pTopFace,-20,-20,0),v10,fine,tCalibTemp\WObj:=wobj0;
           pTemp := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace1_P2, offs(pTemp,0,0,-10), v5, tCalibTemp\WObj:=wobj0;         
           !Third Point of First Face
           MoveL Offs(pTopFace,-14,10,0),v10,fine,tCalibTemp\WObj:=wobj0;
           SearchL\SStop, di3D_Touch, pFace1_P3, offs(pTemp,0,0,-10), v5, tCalibTemp\WObj:=wobj0;  
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
          
           reg4 :=0;
        ENDIF
        
    ENDPROC
    
    PROC progFace2()
        
        TPErase;
        TPWrite "Locate probe near front face. Select this point as the lowest wrt the top"; 
        reg3 := 0;
        TPReadFK reg3,"Is the probe behind or in front of the face? (wrt Robot home frame)","Behind",stEmpty,stEmpty,stEmpty,"Front"; 
        
        IF reg3 = 1 THEN
           !First Point of First Face
           pFrontFace := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace2_P1, offs(pFrontFace,0,-20,0), v5, tCalibTemp\WObj:=wobj0;
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
           !Second Point of First Face
           MoveL Offs(pTopFace,-20,0,20),v10,fine,tCalibTemp\WObj:=wobj0;
           pTemp := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace2_P2, offs(pTemp,0,-20,0), v5, tCalibTemp\WObj:=wobj0;         
           !Third Point of First Face
           MoveL Offs(pTopFace,13,0,10),v10,fine,tCalibTemp\WObj:=wobj0;
           SearchL\SStop, di3D_Touch, pFace2_P3, offs(pTemp,0,-20,0), v5, tCalibTemp\WObj:=wobj0;  
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
           reg3 :=0;
        ENDIF
        
        IF reg3 = 5 THEN
           !First Point of First Face
           pFrontFace := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace2_P1, offs(pFrontFace,0,20,0), v5, tCalibTemp\WObj:=wobj0;
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
           !Second Point of First Face
           MoveL Offs(pTopFace,-20,0,20),v10,fine,tCalibTemp\WObj:=wobj0;
           pTemp := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace2_P2, offs(pTemp,0,20,0), v5, tCalibTemp\WObj:=wobj0;         
           !Third Point of First Face
           MoveL Offs(pTopFace,13,0,10),v10,fine,tCalibTemp\WObj:=wobj0;
           SearchL\SStop, di3D_Touch, pFace2_P3, offs(pTemp,0,20,0), v5, tCalibTemp\WObj:=wobj0;  
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
           reg3 :=0;
        ENDIF
        
    ENDPROC
    
    PROC progFace3()
        
        TPErase;
        TPWrite "Locate probe near side face. Select this point as the lowest wrt the top"; 
        reg2 := 0;
        TPReadFK reg2,"Probe left or right side of the face? (wrt Robot home frame)","Left",stEmpty,stEmpty,stEmpty,"Right";  
        
        IF reg2 = 1 THEN
           !First Point of First Face
           pSideFace := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace3_P1, offs(pSideFace,-10,0,0), v5, tCalibTemp\WObj:=wobj0;
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
           !Second Point of First Face
           MoveL Offs(pTopFace,0,-20,20),v10,fine,tCalibTemp\WObj:=wobj0;
           pTemp := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace3_P2, offs(pTemp,-10,0,0), v5, tCalibTemp\WObj:=wobj0;         
           !Third Point of First Face
           MoveL Offs(pTopFace,0,17,15),v10,fine,tCalibTemp\WObj:=wobj0;
           SearchL\SStop, di3D_Touch, pFace3_P3, offs(pTemp,-10,0,0), v5, tCalibTemp\WObj:=wobj0;  
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
          
           reg2 :=0;
        ENDIF
        
        IF reg2 = 1 THEN
           !First Point of First Face
           pSideFace := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace3_P1, offs(pSideFace,10,0,0), v5, tCalibTemp\WObj:=wobj0;
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
           !Second Point of First Face
           MoveL Offs(pTopFace,0,-20,20),v10,fine,tCalibTemp\WObj:=wobj0;
           pTemp := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
           SearchL\SStop, di3D_Touch, pFace3_P2, offs(pTemp,10,0,0), v5, tCalibTemp\WObj:=wobj0;         
           !Third Point of First Face
           MoveL Offs(pTopFace,0,17,15),v10,fine,tCalibTemp\WObj:=wobj0;
           SearchL\SStop, di3D_Touch, pFace3_P3, offs(pTemp,10,0,0), v5, tCalibTemp\WObj:=wobj0;  
           MoveL pTopFace,v10,z0,tCalibTemp\WObj:=wobj0;
          
           reg2 :=0;
        ENDIF
    ENDPROC
    
    FUNC pos Step1(robtarget p1, robtarget p2)
        !!This step will generate a vector from the substraction of the first point and the next point
        
        VAR pos v;     
        
        v.x := p2.trans.x - p1.trans.x;
        v.y := p2.trans.y - p1.trans.y;
        v.z := p2.trans.z - p1.trans.z;
       
        RETURN v;
    ENDFUNC

    FUNC pos Step2(pos v1, pos v2)
        !!This step calculates the cross product between two vectors
    
    	VAR pos CP;
        
    	CP.x := v1.y*v2.z - v1.z*v2.y;
    	CP.y := v1.z*v2.x - v1.x*v2.z;
    	CP.z := v1.x*v2.y - v1.y*v2.x;
        
    	RETURN CP; 
    
    ENDFUNC

    FUNC num Step3(pos eq, robtarget p1)
        !!This step calculates the final coeficient to obtain the final result of the equation
    
    	VAR num d;        
    	d := eq.x*p1.trans.x + eq.y*p1.trans.y + eq.z*p1.trans.z;
        TPWrite  ValToStr(eq.x)+"+"+ValToStr(eq.y)+"+"+ValToStr(eq.z)+"="+ValToStr(d);
        RETURN d;
   
    ENDFUNC
       
    FUNC num Matrix_Determinant(pos v1, pos v2, pos v3)
    	VAR num determinante;
    	determinante := v1.x*((v2.y*v3.z)-(v2.z*v3.y)) - v1.y*((v2.x*v3.z)-(v3.x*v2.z)) + v1.z*((v2.x*v3.y)-(v3.x*v2.y));
    	RETURN determinante; 
    ENDFUNC
    
    FUNC pos Matrix_of_Minors_Row1(pos v1, pos v2, pos v3)
        VAR pos TempLinEq;
        TempLinEq.x := ((v2.y*v3.z)-(v3.y*v2.z));
        TempLinEq.y := ((v2.x*v3.z)-(v3.x*v2.z));
        TempLinEq.z := ((v2.x*v3.y)-(v3.x*v2.y));
        RETURN TempLinEq;
    ENDFUNC
    
    FUNC pos Matrix_of_Minors_Row2(pos v1, pos v2, pos v3)
        VAR pos TempLinEq;
        TempLinEq.x := ((v1.y*v3.z)-(v2.y*v1.z));
        TempLinEq.y := ((v1.x*v3.z)-(v3.x*v1.z));
        TempLinEq.z := ((v1.x*v3.y)-(v3.x*v1.y));
        RETURN TempLinEq;
    ENDFUNC
    
    FUNC pos Matrix_of_Minors_Row3(pos v1, pos v2, pos v3)
        VAR pos TempLinEq;
        TempLinEq.x := ((v1.y*v2.z)-(v2.y*v1.z));
        TempLinEq.y := ((v1.x*v2.z)-(v2.x*v1.z));
        TempLinEq.z := ((v1.x*v2.y)-(v2.x*v1.y));
        RETURN TempLinEq;
    ENDFUNC
    
    FUNC pos CoFact_1and3(pos v1)
        VAR pos cofact;
        cofact := v1;
        cofact.y := (-1*v1.y);      
        RETURN cofact;
    ENDFUNC
    
    FUNC pos CoFact_2(pos v1)
        VAR pos cofact;
        cofact := v1;
        cofact.x := (-1*v1.x);
        cofact.z := (-1*v1.z); 
        RETURN cofact;
    ENDFUNC
    
    FUNC pos Adjugating_Row_1(pos v1, pos v2, pos v3)
        VAR pos adjugate;
        adjugate.x := v1.x;
        adjugate.y := v2.x;
        adjugate.z := v3.x;
        RETURN adjugate;
    ENDFUNC
    
    FUNC pos Adjugating_Row_2(pos v1, pos v2, pos v3)
        VAR pos adjugate;
        adjugate.x := v1.y;
        adjugate.y := v2.y;
        adjugate.z := v3.y;
        RETURN adjugate;
    ENDFUNC
    
    FUNC pos Adjugating_Row_3(pos v1, pos v2, pos v3)
        VAR pos adjugate;
        adjugate.x := v1.z;
        adjugate.y := v2.z;
        adjugate.z := v3.z;
        RETURN adjugate;
    ENDFUNC
    
    FUNC pos Inverse_Row(pos v1, num determinante)
        VAR pos inverse;
        inverse.x := v1.x/determinante;
        inverse.y := v1.y/determinante;
        inverse.z := v1.z/determinante;      
        RETURN inverse;
    ENDFUNC
    
    FUNC num Eq_Solver(pos v1, num D{*})
        VAR num coordinate;        
        coordinate := D{1}*v1.x + D{2}*v1.y + D{3}*v1.z;
        RETURN coordinate;
    ENDFUNC
    
    FUNC wobjdata CompensationVector(INOUT wobjdata temporal, num ball_diameter, robtarget p1_Face1, robtarget p1_Face2, robtarget p1_Face3)
        VAR wobjdata work_object;
        work_object := temporal;
        !! Defining the sense of the final compensating vector
        
            !!Defining direction of Y axis
            IF p1_Face1.trans.y-p1_Face2.trans.y < 0 THEN
                temporal.oframe.trans.y := temporal.oframe.trans.y+(ball_diameter/2);
            ELSEIF p1_Face1.trans.y-p1_Face2.trans.y > 0 THEN
                temporal.oframe.trans.y := temporal.oframe.trans.y-(ball_diameter/2);
            ELSE
            ENDIF
            
            !!Defining direction of X axis   
            IF p1_Face3.trans.x-p1_Face1.trans.x < 0 THEN
                temporal.oframe.trans.x := temporal.oframe.trans.x+(ball_diameter/2);
            ELSEIF p1_Face3.trans.x-p1_Face1.trans.x > 0 THEN
                temporal.oframe.trans.x := temporal.oframe.trans.x-(ball_diameter/2);
            ELSE
            ENDIF
            
            !!Moving Z negative
                temporal.oframe.trans.z := temporal.oframe.trans.z-(ball_diameter/2);
        
            !! Define magnitude from World
            TPWrite "Distance from world to work object is "+ValToStr(VectMagn(temporal.oframe.trans))+"mm"; 
            TPWrite "Coordinates:"+ValToStr(temporal.oframe.trans.x)+","+ValToStr(temporal.oframe.trans.y)+","+ValToStr(temporal.oframe.trans.z)+"";
              
            !! Defining orientation
            
            !!  There are multiple methods to define rotations (because it is a coordinate frame) and orientation (for the Tool frame) as seen before
            !! We will utilize wobj as origin.
            !! In this case, due to the fact we have the top plane and side plane equation, we can extrapolate towards 
            !! the positive direction of the front plane.
                
                !! Method 1a - Finding angle between planes
                
                angle_Y := AngleFinder(PlaneXY,Normal_Vector_Face1);
                angle_Z := AngleFinder(PlaneXZ,Normal_Vector_Face3);
                angle_X := AngleFinder(PlaneYZ,Normal_Vector_Face2);
                
             
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                    
                
            
                !! Method 2 (In progress) - Creating two more points
                !!Creating normal using Cross Product
                CrossProduct := CrossProd(Normal_Vector_Face1,Normal_Vector_Face3);            
                !!Obtaining new X1_positive point
                X1_Pos := CalcPoint(CrossProduct,temporal);
                
                !! Sharing variable 
                !!Creating normal using Cross Product
                CrossProduct := CrossProd(Normal_Vector_Face2,Normal_Vector_Face1);
                !!Obtaining new Y1_positive point
                Y1_Pos := CalcPoint(CrossProduct,temporal);
            
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
        
        work_object := temporal;        
               
        work_object.oframe.rot := OrientZYX(angle_Z, angle_Y, angle_X);
        temporal := work_object;
                
        RETURN temporal;
    ENDFUNC
    
    FUNC pos CrossProd(pos v2, pos v3)
        !! For a 2x2 matrix
    	VAR pos CP;
        VAR pos v1 := [1,1,1];
    	CP.x := v1.x*((v2.y*v3.z)-(v2.z*v3.y)); 
        CP.y := (-1)* v1.y*((v2.x*v3.z)-(v3.x*v2.z)); 
        CP.z := v1.z*((v2.x*v3.y)-(v3.x*v2.y));
    	RETURN CP; 
    ENDFUNC
    
    FUNC pos CalcPoint(pos Line, wobjdata Origin)
        VAR pos point;
        !!Distance to define the point
        VAR num distance := 100;
        point.x := Origin.oframe.trans.x + (Line.x*distance);
        point.y := Origin.oframe.trans.y + (Line.y*distance);
        point.z := Origin.oframe.trans.z + (Line.z*distance);
               
        RETURN point;
    ENDFUNC
    
    
    FUNC num AngleFinder(pos Plane1, pos Plane2)
        VAR num angle;
        !! Angle finder between two planes
        angle := ACos ( (Plane1.x*Plane2.x + Plane1.y*Plane2.y + Plane1.z*Plane2.z) / ( Sqrt( Pow(Plane1.x,2) + Pow(Plane1.y,2) + Pow(Plane1.z,2) ) * Sqrt( Pow(Plane2.x,2) + Pow(Plane2.y,2) + Pow(Plane2.z,2)  ) ));
        
        RETURN angle;        
    ENDFUNC
    
ENDMODULE