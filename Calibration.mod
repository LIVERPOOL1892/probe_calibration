MODULE Calibration
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ARTC!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Walter Frank Pintor Ortiz!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!25th July 2016!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Probing Calibration System!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
  !Add-ons
  VAR clock clocktime; !!Initializing our clock conditions
  VAR NUM time; !!Defining the variable for clock
  VAR errnum errvar; !!Flag indicator

  !!Variables for calibration of work object in the future
  VAR num CalibMaxErr;
  VAR num CalibMeanErr;
  !This work object will be updated after measuring the defined components
  !TASK PERS wobjdata wob1:=[FALSE,TRUE,"",[[700.00,50.00,230.00],[1,0,0,0]],[[0,0,0],[1,0,0,0]]]; !!Initial Stage for wobj1
  
  !!Define size of number of measurements
  CONST num nMeasurements := 25;
  
  !!Define physical theoretical lenghts and dimensions
  CONST num nRadRedSphere := 25; !!25mm for the Red calibration sphere
  CONST NUM nProbeRadius := 3; !!3mm for the currect probe version  
  
  !!Variable to store the location on top of the RED calibration sphere
  VAR robtarget pTopJogged;
  VAR robtarget pTopLearned;
  !!Temporal variable that helps store locations
  VAR robtarget pTemp;
  !!Temporal variable that allocates distances
  VAR num nDistOff := 0;
  VAR num nDistOff45 := 0;
  VAR num nDistOff67 := 0;
  VAR num nDistOff90 := 0;
  
  !!Initialize Positions
  VAR robtarget pFound;
  VAR robtarget pEndPoint;
  
  !!All points
  PERS robtarget pSphere{nMeasurements};
  PERS pose pPose{nMeasurements};
  
  !!Home in joints
  var jointtarget jhome:=[[0,0,0,0,0,0],[9E9,9E9,9E9,9E9,9E9,9E9]];
  
    !!Temporal tool
    PERS tooldata tCalibTemp:=[TRUE,[[-19.6916,-0.334428,533.568],[1,0,0,0]],[53.5,[-35.4,0.5,227.3],[1,0,0,0],4.097,5.318,0]];
    !!Initialize tool
    PERS tooldata tMeasure;
    
    
PROC Routine()
    !!This routine will provide 25 measurements using a probing system, trying to simulate the operation of the CMM machine.  
    
    TPErase; !!Cleans the Pendant
    TPWrite "Welcome to the Calibration Program";
    TPWrite "Remember to move carefully!";
    TPWrite "                           ";
    
    !!Move the component as close as possible to the top of the RED calibration sphere
    reg5 := 0;
    TPReadFK reg5,"Locate the probe on top of the RED calibration sphere","Done",stEmpty,stEmpty,stEmpty,"Move home and try again!"; !!\MaxTime:=60\BreakFlag:= errvar; !!In case of error flag display
    !OR errvar = ERR_TP_MAXTIME
    IF reg5 = 1 THEN
        !!Starts the clock
        CLKStart clocktime;
        !!Stores location as ptop
        pTopJogged := CRobT(\Tool:=tCalibTemp \WObj:=wobj0);
        reg5 := 0;
        !!Goes to routine, where all logic for the probe motion is given
        probing;
    ENDIF
    IF reg5 = 5 THEN
        MoveAbsJ jhome,v20,z1,tool0\WObj:=wobj0;
        reg5 := 0;
    ENDIF
          
    ConfL\Off;
    
    CLKStop clocktime; !!Stoping clock
    time := ClkRead(clocktime); !!Saving time as a variable
    TPWrite "Time Elapsed for the selected Calibration Process is ="\num:=time; !!Displaying time elapsed
ENDPROC 


PROC Probing()
  
    !!Initialize variables for TCP
    VAR pose tcppose:=[[0,0,0],[1,0,0,0]];
    VAR pose tcpoffsetpose:=[[0,0,0],[1,0,0,0]];
    VAR pose newtcppose:=[[0,0,0],[1,0,0,0]];

    !!This program will provide the logic for the probe motion and will call the store routine, to save the rest of the points.
    !!By default, 25 points will be stored.
    
    !!Adjust point
    pEndPoint := offs(pTopJogged,0,0,-10);
    
    !!Adjusting tcp of tool with the probe radius defined
    tMeasure:=tCalibTemp;
    tcppose:=tCalibTemp.tframe;
    tcpoffsetpose.trans:=[0,0,-nProbeRadius];
    tcpoffsetpose.rot:=[1,0,0,0];
    newtcppose:=PoseMult(tcppose,tcpoffsetpose);
    tMeasure.tframe:=newtcppose;
    
    !!Turn on device and pulse signal
    TurnOnProbe;
    
    !!Searching down the surface!!
    !SearchL\SStop, di3D_Touch, pFound, pEndPoint, v5, tMeasure\WObj:=wobj0;
    !!Saving pFound with the current data
    pFound := CRobT();
    TPWrite "The first point is ="\Pos:=pFound.trans; !!Displaying position elapsed
    TPWrite "with orientation ="\Orient:=pFound.rot; !!Displaying orientation elapsed
    !!Store in our arrays
    pSphere{0}:=pFound;
    
    !!Move up
    MoveL pTopJogged,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Capture the first four points
    Capture4(pTopJogged);
    !!Time to center the probe 
    CenterIt;
 
    !!After centering the initial point, it is possible to start the routine for measuring the 25 points
    !!Since we already have the first 5 points, let us continue with the remaining 20
    
    Remaining20;
    
    !!Turn off probe
    TurnOffProbe;
    
    
ENDPROC


PROC Capture4(robtarget pTop)

    !!Define number of planes for half of the hemispheres
    VAR num nCuttingPlanes:=4;
    !!Angles defined for the planes created
    VAR num nAngles := 90/nCuttingPlanes;
    !!Variable for error of height of points measured
    VAR num nErrorHeight := 0;
    
    !!Define offset for the first 22.5 degrees
    nDistOff := nRadRedSphere*Sin(nAngles);
    
    
    !!Commence to probe the next four points
    !!First point
    MoveL offs(pTop,nDistOff,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{1}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTop,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Second point
    MoveL offs(pTop,-nDistOff,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{2}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTop,v10,z0,tMeasure\WObj:=wobj0;    
    
    !!Third point
    MoveL offs(pTop,0,nDistOff,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{3}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTop,v10,z0,tMeasure\WObj:=wobj0; 
    
    !!Fourth point
    MoveL offs(pTop,0,-nDistOff,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{4}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTop,v10,z0,tMeasure\WObj:=wobj0; 
    
        
ENDPROC

PROC CenterIt()

    CONST num nErrorinX := 0;
    CONST num nErrorinY := 0;
    
    VAR num nDeltaZa := 0;
    VAR num nDeltaZb := 0;
    VAR num nDeltaZc := 0;
    VAR num nDeltaZd := 0;
    
    !!Aproach Rate
    VAR num nRateX := 1;
    VAR num nRateY := 1;
    
    !!Ratio
    VAR num nRatioX := 1;
    VAR num nRatioY := 1;
    
    !!Understand difference in height among the points and the initial point read
    nDeltaZa := pSphere{0}.trans.z - pSphere{1}.trans.z;
    nDeltaZb := pSphere{0}.trans.z - pSphere{2}.trans.z;
    nDeltaZc := pSphere{0}.trans.z - pSphere{3}.trans.z;
    nDeltaZd := pSphere{0}.trans.z - pSphere{4}.trans.z;
    
    !!Understand error of the different points of the Z direction 
    nErrorinX := nDeltaZa-nDeltaZb;
    nErrorinY := nDeltaZc-nDeltaZd;
    
    
    !!Ensuring we always have positive and bigger than one values
    nRatioX := abs(nDeltaZa/nDeltaZb);
    nRatioY := abs(nDeltaZc/nDeltaZd);
    IF nRatioX<1 THEN
        nRatioX := abs(nDeltaZb/nDeltaZa);
    ENDIF
    IF nRatioY<1 THEN
        nRatioY := abs(nDeltaZd/nDeltaZc);
    ENDIF
    
    !!Generating a simple form for creating faster speeds of approaches
    !!A general line equation is placed in order to define speeds after identifying the height differences for the points 
    !!of the same axis.
    !!This is why, there is two rates for learning as we don't know how far we are from the real center in X and Y.
    
    
    nRateX := 0.08*nRatioX - 0.07;
    nRateY := 0.08*nRatioY - 0.07;
    
    !!Correct X direction
    !!Towards Positive X
    IF nErrorinX>0.05 THEN  
    WHILE nErrorinX>0.05 DO
        pTopLearned.trans.x := pTopLearned.trans.x + (nDistOff*nRateX);
        Capture4(pTopLearned);
    ENDWHILE
    ENDIF
    !!Towards Negative X
    IF nErrorinX<0.05 THEN
    WHILE nErrorinX<0.05 DO
        pTopLearned.trans.x := pTopLearned.trans.x - (nDistOff*nRateX);
        Capture4(pTopLearned);
    ENDWHILE
    ENDIF
    
    
    !!Correct Y direction
    !!Towards Positive Y
    IF nErrorinY>0.05 THEN  
    WHILE nErrorinY>0.05 DO
        pTopLearned.trans.y := pTopLearned.trans.y + (nDistOff*nRateY);
        Capture4(pTopLearned);
    ENDWHILE
    ENDIF
    !!Towards Negative Y
    IF nErrorinY<0.05 THEN
    WHILE nErrorinY<0.05 DO
        pTopLearned.trans.y := pTopLearned.trans.y - (nDistOff*nRateY);
        Capture4(pTopLearned);
    ENDWHILE
    ENDIF
    
    
    
ENDPROC

PROC Remaining20()
    VAR num nOffsetZ90 := 0;
    
    !!Move above, to the learned more appropiate center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Define offset for the Second 45 degrees
    nDistOff45 := nRadRedSphere*Sin(45);
    
    !!Commence to probe the next twenty points
    !!Fifth point
    MoveL offs(pTopLearned,0,-nDistOff45,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{5}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Sixth point
    MoveL offs(pTopLearned,nRadRedSphere/2,-nRadRedSphere/2,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{6}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Seventh point
    MoveL offs(pTopLearned,nDistOff45,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{7}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Eighth point
    MoveL offs(pTopLearned,nRadRedSphere/2,nRadRedSphere/2,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{8}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Ninth point
    MoveL offs(pTopLearned,0,nDistOff45,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{9}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Tenth point
    MoveL offs(pTopLearned,-nRadRedSphere/2,nRadRedSphere/2,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{10}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Eleventh point
    MoveL offs(pTopLearned,-nDistOff45,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{11}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Twelfth point
    MoveL offs(pTopLearned,-nRadRedSphere/2,-nRadRedSphere/2,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{12}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    
    !!!!!Time to continue with the next layer of points!!!!
    !! Layer of 67.5 degree
    
    !!Define offset for the Second 67.5 degrees
    nDistOff67 := nRadRedSphere*Sin(67.5);
    
    !!Thirteenth point
    MoveL offs(pTopLearned,0,-nDistOff67,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{13}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Fourteenth point
    MoveL offs(pTopLearned,nDistOff67,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{14}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Fifteenth point
    MoveL offs(pTopLearned,0,nDistOff67,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{15}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Sixteenth point
    MoveL offs(pTopLearned,-nDistOff67,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,0,-10), v5, tMeasure\WObj:=wobj0;
    pSphere{15}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!!!!Time to continue with the next layer of points!!!!
    !! Layer of 90 degree
    !! Extend more than the radius, so we can come in the X or Y plane
    
    !!Distance at 90 degree !! Basically, it is the radius, plus some margin of safety
    nDistOff90 := nRadRedSphere + nRadRedSphere*0.1;
    
    !!Seventeenth point
    MoveL offs(pTopLearned,0,-nDistOff90,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,5,0), v5, tMeasure\WObj:=wobj0;
    pSphere{17}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Eigteenth point
    MoveL offs(pTopLearned,nDistOff90,-nDistOff90,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,-5,5,0), v5, tMeasure\WObj:=wobj0;
    pSphere{18}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Ninteenth point
    MoveL offs(pTopLearned,nDistOff90,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,-5,0,0), v5, tMeasure\WObj:=wobj0;
    pSphere{19}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Twentieth point
    MoveL offs(pTopLearned,nDistOff90,nDistOff90,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,-5,-5,0), v5, tMeasure\WObj:=wobj0;
    pSphere{20}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Twenty-first point
    MoveL offs(pTopLearned,0,nDistOff90,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,0,-5,0), v5, tMeasure\WObj:=wobj0;
    pSphere{21}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Twenty-second point
    MoveL offs(pTopLearned,-nDistOff90,nDistOff90,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,5,-5,0), v5, tMeasure\WObj:=wobj0;
    pSphere{22}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Twenty-third point
    MoveL offs(pTopLearned,-nDistOff90,0,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,5,0,0), v5, tMeasure\WObj:=wobj0;
    pSphere{23}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
    !!Twenty-fourth point
    MoveL offs(pTopLearned,-nDistOff90,-nDistOff90,0),v10,z0,tMeasure\WObj:=wobj0;
    !!Store air point temporarily
    pTemp := CRobT();
    nOffsetZ90 := abs(pTopLearned.trans.z - pSphere{0}.trans.z - nRadRedSphere);!! This is to find out how much it is required to go down
    MoveL offs(pTemp,0,0,nOffsetZ90),v5,z0,tMeasure\WObj:=wobj0;
    !!Search and save
    SearchL\SStop, di3D_Touch, pFound, offs(pTemp,5,5,0), v5, tMeasure\WObj:=wobj0;
    pSphere{24}:=pFound;
    !!Move up
    MoveL pTemp,v10,z0,tMeasure\WObj:=wobj0;
    !!Move back to meantime center
    MoveL pTopLearned,v10,z0,tMeasure\WObj:=wobj0;
    
ENDPROC


ENDMODULE