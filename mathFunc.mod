MODULE mathFunc


! CONST num PI := 3.14159265358979;

! Matrices are defined in num arrays in column major
! i.e. for
! [a d g]
! [b e h]
! [c f i]
! The corresponding num array given by mat := {a, b, c, d, e, f, g, h, i};
! WARNING: No checks are performed to ensure matrix multiplications, all arrays are assumed to be of size 9.

FUNC num Determinant(pos v1, pos v2, pos v3)
	VAR num det;
	det := v1.x*v2.y*v3.z + v2.x*v3.y*v1.z + v3.x*v1.y*v2.z;
	det := det - v1.x*v3.y*v2.z - v2.x*v1.y*v3.z - v3.x*v2.y*v1.z;
	RETURN det; 
ENDFUNC

FUNC pos LinearEquations3(pos v1, pos v2, pos v3, pos b)
	VAR num det;
	VAR pos answer;
	det := Determinant(v1,v2,v3);
	answer.x := Determinant(b,v2,v3)/det;
	answer.y := Determinant(v1,b,v3)/det;
	answer.z := Determinant(v1,v2,b)/det;
	RETURN answer; 
ENDFUNC

FUNC pos CrossProduct(pos v1, pos v2)
	VAR pos answer;
	answer.x := v1.y*v2.z - v1.z*v2.y;
	answer.y := v1.z*v2.x - v1.x*v2.z;
	answer.z := v1.x*v2.y - v1.y*v2.x;
	RETURN answer; 
ENDFUNC

FUNC num DotProduct(pos v1, pos v2)
	VAR num answer;
	answer := v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
	RETURN answer; 
ENDFUNC

FUNC pos ArcCentre(pos p1, pos p2, pos p3)
	VAR pos p12;
	VAR pos p32;
	VAR pos n1;
	VAR pos n2;
	VAR pos n3;
	VAR pos v1;
	VAR pos v2;
	VAR pos v3;
	VAR pos b;
	VAR pos centre;
		
	p12 := 0.5*(p1+p2); 
	p32 := 0.5*(p3+p2);
	
	n1 := Nomalize(p1-p2); 
	n2 := Nomalize(p3-p2); 
	n3 := Nomalize(CrossProduct(n1,n2));
	
	v1.x := n1.x;
	v1.y := n2.x;
	v1.z := n3.x;

	v2.x := n1.y;
	v2.y := n2.y;
	v2.z := n3.y;
	
	v3.x := n1.z;
	v3.y := n2.z;
	v3.z := n3.z;

	b.x := n1.x*p12.x + n1.y*p12.y + n1.z*p12.z;
	b.y := n2.x*p32.x + n2.y*p32.y + n2.z*p32.z;
	b.z := n3.x*p2.x + n3.y*p2.y + n3.z*p2.z;
	
	centre := LinearEquations3(v1, v2, v3, b);
	RETURN centre; 
ENDFUNC

FUNC pos Nomalize(pos v)
	RETURN v*(1/VectMagn(v));
ENDFUNC

FUNC pos PointExtend(pos v1, pos v2, num len)
	RETURN v2 + len*Nomalize(v2-v1);
ENDFUNC

! Scales the vector v1 by gain

FUNC pos scaleVec(pos v1, num gain)
  VAR pos v2;
  v2.x := v1.x*gain;
  v2.y := v1.y*gain;
  v2.z := v1.z*gain;
  RETURN v2;
ENDFUNC


! Adds vector v1 and v2

FUNC pos addVec(pos v1, pos v2)
  VAR pos v3;
  v3.x := v1.x + v2.x;
  v3.y := v1.y + v2.y;
  v3.z := v1.z + v2.z;
  RETURN v3;
ENDFUNC

! Adds vector v1, v2 and v3

FUNC pos addVec2(pos v1, pos v2, pos v3)
  VAR pos v4;
  v4.x := v1.x + v2.x + v3.x;
  v4.y := v1.y + v2.y + v3.y;
  v4.z := v1.z + v2.z + v3.z;
  RETURN v4;
ENDFUNC

! Scales the matrix mat1 by gain and puts the result in mat2

PROC scaleMat(num inMat{*}, VAR num outMat{*}, num gain, num n)
  FOR i FROM 1 TO n DO
    outMat{i} := inMat{i}*gain;
  ENDFOR
ENDPROC

! Multiplies Matrix mat1 by mat2 and puts the results in mat3

PROC matMult(VAR num firstMat{*}, VAR num secondMat{*}, VAR num outMat{*}, num row, num col, num col2)
	FOR i FROM 1 TO row DO
		FOR j FROM 1 TO col2 DO
			outMat{i + (j-1)*row} := 0;
			FOR k FROM 1 TO col DO
				outMat{i + (j-1)*row} := outMat{i + (j-1)*row} + firstMat{(k-1)*row + i}*secondMat{(j-1)*col + k};
			ENDFOR
		ENDFOR
	ENDFOR
ENDPROC

! Adds 2 matrices mat1 to mat2 and puts result in mat3

PROC matAdd(VAR num mat1{*}, VAR num mat2{*}, VAR num mat3{*}, num row, num col)
  FOR i FROM 1 TO row*col DO
    mat3{i} := mat1{i} + mat2{i};
  ENDFOR
ENDPROC

! Adds 3 matrices mat1 to mat3 and puts result in mat4

PROC matAdd2(VAR num mat1{*}, VAR num mat2{*}, VAR num mat3{*}, VAR num mat4{*}, num row, num col)
  FOR i FROM 1 TO row*col DO
    mat4{i} := mat1{i} + mat2{i} + mat3{i};
  ENDFOR
ENDPROC

FUNC num sign(num a)
  IF a >= 0 THEN
    RETURN 1;
  ENDIF
  RETURN -1;
ENDFUNC


FUNC num abs(num a)
  RETURN a*sign(a);
ENDFUNC

PROC orientToRot(orient orient1, VAR num mat1{*})

	mat1{1} := orient1.q1*orient1.q1 + orient1.q2*orient1.q2 - orient1.q3*orient1.q3 - orient1.q4*orient1.q4;
	mat1{4} := 2*orient1.q2*orient1.q3 - 2*orient1.q1*orient1.q4;
	mat1{7} := 2*orient1.q1*orient1.q3 + 2*orient1.q2*orient1.q4;

	mat1{2} := 2*orient1.q1*orient1.q4 + 2*orient1.q2*orient1.q3;
	mat1{5} := orient1.q1*orient1.q1 - orient1.q2*orient1.q2 + orient1.q3*orient1.q3 - orient1.q4*orient1.q4;
	mat1{8} := 2*orient1.q3*orient1.q4 - 2*orient1.q1*orient1.q2;

	mat1{3} := 2*orient1.q2*orient1.q4 - 2*orient1.q1*orient1.q3;
	mat1{6} := 2*orient1.q1*orient1.q2 + 2*orient1.q3*orient1.q4;
	mat1{9} := orient1.q1*orient1.q1 - orient1.q2*orient1.q2 - orient1.q3*orient1.q3 + orient1.q4*orient1.q4;
ENDPROC

! Converts rotation matrix mat1 into ABB quarternion

FUNC orient rotToOrient(VAR num mat1{*})
	VAR orient orient1;
	orient1.q1 := Sqrt(mat1{1} + mat1{5} + mat1{9} + 1)/2;
	orient1.q2 := (Sqrt(mat1{1} - mat1{5} - mat1{9} + 1)/2)*sign(mat1{6} - mat1{8});
	orient1.q3 := (Sqrt(mat1{5} - mat1{1} - mat1{9} + 1)/2)*sign(mat1{7} - mat1{3});
	orient1.q4 := (Sqrt(mat1{9} - mat1{1} - mat1{5} + 1)/2)*sign(mat1{2} - mat1{4});
	RETURN orient1;
ENDFUNC

FUNC orient TransMatToOrient(VAR num mat1{*})
	VAR orient orient1;
	orient1.q1 := Sqrt(mat1{1} + mat1{6} + mat1{11} + 1)/2;
	orient1.q2 := (Sqrt(mat1{1} - mat1{6} - mat1{11} + 1)/2)*sign(mat1{7} - mat1{10});
	orient1.q3 := (Sqrt(mat1{6} - mat1{1} - mat1{11} + 1)/2)*sign(mat1{9} - mat1{3});
	orient1.q4 := (Sqrt(mat1{11} - mat1{1} - mat1{6} + 1)/2)*sign(mat1{2} - mat1{5});
	RETURN orient1;
ENDFUNC

FUNC num index(num row, num col, num totalRow)

  RETURN (row + (col - 1)*(totalRow));
ENDFUNC


PROC swapRow(VAR num mat1{*}, num i, num j, num n)
	
	VAR num tmp;
	VAR num k;
	
	FOR k FROM 1 to n DO
		tmp := mat1{(i - 1)*n + k};
		mat1{(i - 1)*n + k} := mat1{(j - 1)*n + k};
		mat1{(j - 1)*n + k} := tmp;
	ENDFOR

ENDPROC

PROC makeValid(VAR num inMat{*}, VAR num outMat{*}, num i, num n)
	VAR num j;
	VAR num check;
	check := abs(inMat{(i-1)*n + i});
	IF check < 0.000001 THEN
		FOR j FROM i+1 TO n DO
			check := abs(inMat{(j-1)*n + i});
			IF check > 0.000001 THEN
				swapRow inMat, i, j, n;
				swapRow outMat, i, j, n;
				RETURN;
			ENDIF
		ENDFOR	
	ENDIF
ENDPROC

PROC inv(num inMat{*}, VAR num outMat{*}, num n)

	VAR num i;
	VAR num j;
	VAR num k;
	VAR num tmp;
	VAR num check;

	FOR i FROM 1 TO n*n DO
		outMat{i} := 0;
	ENDFOR

	FOR i FROM 1 TO n DO
		outMat{(i-1)*n + i} := 1;
	ENDFOR

	FOR i FROM 1 TO n DO
	
		makeValid inMat, outMat, i, n;
		check := abs(inMat{(i-1)*n + i});
		IF check < 0.000001 THEN
			RETURN;
		ENDIF

		tmp := inMat{(i - 1)*n + i};
		FOR j FROM 1 To n DO
			inMat{(i - 1)*n + j} := inMat{(i - 1)*n + j}/tmp;
			outMat{(i - 1)*n + j} := outMat{(i - 1)*n + j}/tmp;
		ENDFOR
		
		IF i > 1 THEN
			FOR j FROM 1 To i-1 DO
				tmp := inMat{(j - 1)*n + i};
				FOR k FROM 1 To n DO
					inMat{(j - 1)*n + k} := inMat{(j - 1)*n + k} - tmp*inMat{(i - 1)*n + k};
					outMat{(j - 1)*n + k} := outMat{(j - 1)*n + k} - tmp*outMat{(i - 1)*n + k};
    			ENDFOR
			ENDFOR
		ENDIF
	
		IF i < n THEN
			FOR j FROM i+1 To n DO
				tmp := inMat{(j - 1)*n + i};
				FOR k FROM 1 To n DO
					inMat{(j - 1)*n + k} := inMat{(j - 1)*n + k} - tmp*inMat{(i - 1)*n + k};
					outMat{(j - 1)*n + k} := outMat{(j - 1)*n + k} - tmp*outMat{(i - 1)*n + k};
    			ENDFOR
			ENDFOR	
		ENDIF
	
	ENDFOR

ENDPROC

PROC transpose(VAR num inMat{*}, VAR num outMat{*}, num row, num col)
	VAR num i;
	VAR num j;
	
	FOR i FROM 1 TO row DO
		FOR j FROM 1 TO col DO
			outMat{(i - 1)*col + j} := inMat{(j-1)*row + i};
		ENDFOR
	ENDFOR
ENDPROC


FUNC num norm(num vec{*}, num len)
	VAR num tmp;
	VAR num i;
	
	tmp := 0;
	FOR i FROM 1 TO len DO
		tmp := tmp + vec{i}*vec{i};
	ENDFOR

	RETURN Sqrt(tmp);
	
ENDFUNC

PROC HorzCat(num firstMat{*}, num secondMat{*}, VAR num outMat{*}, num col1, num col2, num rows)
	VAR num i;
	VAR num offset;

	offset := col1*rows;
	
	FOR i FROM 1 TO offset DO
		outMat{i} := firstMat{i};
	ENDFOR


	FOR i FROM 1 TO col2*rows DO
		outMat{offset + i} := secondMat{i};
	ENDFOR

ENDPROC

PROC VertCat(num firstMat{*}, num secondMat{*}, VAR num outMat{*}, num cols, num row1, num row2)
	VAR num i;
	VAR num j;
	
	FOR i FROM 1 TO cols DO
		FOR j FROM 1 TO row1 DO
			outMat{index(j, i, row1+row2)} := firstMat{i};
		ENDFOR
		FOR j FROM 1 TO row2 DO
			outMat{index(row1 + j , i, row1+row2)} := secondMat{index(j, i, row2)};
		ENDFOR
	ENDFOR
ENDPROC


PROC declareIdentity(VAR num mat{*}, num size)
	VAR num i;
	
	FOR i FROM 1 TO size*size DO
		mat{i} := 0;
	ENDFOR

	FOR i FROM 1 TO size DO
		mat{(i-1)*size + i} := 1;
	ENDFOR

ENDPROC

FUNC num SinRad(num theta)
	
	RETURN Sin((theta*180.0)/PI);
	
ENDFUNC

FUNC num CosRad(num theta)
	
	RETURN Cos((theta*180.0)/PI);
	
ENDFUNC

PROC RotX(num theta, VAR num T{*})
	
	VAR num i;
	
	FOR i FROM 1 TO 16 DO
		T{i} := 0;
	ENDFOR
	
	T{1} := 1;
	T{6} := CosR(theta);
	T{7} := SinR(theta);
	T{10} := -SinR(theta);
	T{11} := CosR(theta);
	T{16} := 1;
	
ENDPROC

PROC RotY(num theta, VAR num T{*})
	
	VAR num i;
	
	FOR i FROM 1 TO 16 DO
		T{i} := 0;
	ENDFOR
	
	T{1} := CosR(theta);
	T{3} := SinR(theta);
	T{6} := 1;
	T{9} := -SinR(theta);
	T{11} := CosR(theta);
	T{16} := 1;
	
ENDPROC

PROC RotZ(num theta, VAR num T{*})
	
	VAR num i;
	
	FOR i FROM 1 TO 16 DO
		T{i} := 0;
	ENDFOR
	
	T{1} := CosR(theta);
	T{2} := -SinR(theta);
	T{5} := -SinR(theta);
	T{6} := CosR(theta);
	T{11} := 1;
	T{16} := 1;
	
ENDPROC

FUNC num Normalize(pos input)

	VAR num tmp;
	tmp:=sqrt(input.x*input.x+input.y*input.y+input.z*input.z);
	RETURN tmp;
	
ENDFUNC

FUNC pos NormalizeVec(pos vecIn)

	VAR num tmp;
	VAR pos result;
	
	result:=vecIn;
	tmp:=sqrt(vecIn.x*vecIn.x+vecIn.y*vecIn.y+vecIn.z*vecIn.z);
	IF tmp>0 THEN
		result.x:=vecIn.x/tmp;
		result.y:=vecIn.y/tmp;
		result.z:=vecIn.z/tmp;
	ENDIF
	RETURN result;
	
ENDFUNC

PROC formTransMat(pos X, pos Y, pos Z, pos Position, VAR num outMat{*})

	outMat{1}:=X.x;		outMat{5}:=Y.x;		outMat{9}:=Z.x;		outMat{13}:=Position.x;
	outMat{2}:=X.y;		outMat{6}:=Y.y;		outMat{10}:=Z.y;	outMat{14}:=Position.y;
	outMat{3}:=X.z;		outMat{7}:=Y.z;		outMat{11}:=Z.z;	outMat{15}:=Position.z;
	outMat{4}:=0;		outMat{8}:=0;		outMat{12}:=0;		outMat{16}:=1;

ENDPROC


FUNC pos calcVector(pos fromPt, pos toPt)

	VAR pos res;
	res:=toPt-fromPt;
	res:=NormalizeVec(res);
	return res;
	
ENDFUNC


	
ENDMODULE