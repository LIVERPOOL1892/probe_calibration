

MODULE ProbeOperational
	PROC TurnOnProbe()
		TurnOnRMP;
		ProbeStart;
	ENDPROC

	PROC TurnOffProbe()
		ProbeStop;
		TurnOffRMP;
	ENDPROC

	PROC TurnOnRMP()
		!Turn on the RMP(Radio Machine Interface)
		!SetDO DO_2, 1;
		SetDO Probe_DO_1, 1;
		SetDO Probe_DO_2, 1;
		WaitTime 20;
	ENDPROC

	PROC TurnOffRMP()
		!Turn off the RMP(Radio Machine Interface)
		SetDO Probe_DO_1, 0;
		SetDO Probe_DO_2, 0;
	ENDPROC

	PROC ProbeStart()
		!Turn on the probe using pulse signal
		PulseDO \High \PLength:=0.5, Probe_DO_3;
		WaitTime 18;
	ENDPROC

	PROC ProbeStop()
		!Turn off the probe using pulse signal
		PulseDO \High \PLength:=0.5, Probe_DO_3;
		WaitTime 3;
	ENDPROC

ENDMODULE